class C_Gaussian_onedimension(C_Dataset_pt):
    def get_raw_data(self,means,stds,sizes):
        outputs = []
        o = np.random.normal(means[0],stds[0],size=sizes[0])
        outputs.append(o)
        o = np.random.normal(means[1],stds[1],size=sizes[1])
        outputs.append(o)

        outputs = np.concatenate(outputs,-1)
        np.random.shuffle(outputs)
        return outputs
    def get_numpy_data(self,*args,**kargs):
        return self.get_raw_data(*args,**kargs)

    def plot_data(self,data):
        l = len(data)
        x = np.linspace(0,100,num=l)
        plt.plot(x,data,'.')
        plt.show()

        # hist = np.histogram(data,bins=100)
        plt.hist(data,bins=100)
        plt.show()

def EM_univariate(data,k,algorithm=0,threshold=1e-9):
    data = np.array(data,dtype=np.float32)
    assert len(data.shape) == 1

    Emean = np.random.choice(data,size=k,replace=False)
    print("initial mean select",Emean)
    Ew = [1./k] * k
    Evar = [np.var(data)] * k
    data = np.expand_dims(data,0)
    def Expectationn(data,Emean,Evar,Ew):
        data = np.transpose(data)
        a = Ew * st.norm(Emean,np.sqrt(Evar)).pdf(data)
        ## noting: if we use Evar instead of np.sqrt(Evar), expected shape will be widthened
        ##         ,so the mean of prediction will close to mean of true components of distribution
        a = np.transpose(a)
        o =   a / np.sum(a,axis=0,keepdims=True)
        return o

    def Maximization(data,Egamma,epsilon=1e-9):
        assert len(Egamma.shape) == 2
        Nw = np.mean(Egamma,axis=1)
        Nmean = np.sum(Egamma*data, -1) / (np.sum(Egamma,-1) + epsilon)
        Nvar = np.sum(Egamma*(data-np.expand_dims(Nmean,-1))**2,-1) / (np.sum(Egamma,-1)+epsilon)
        return Nmean,Nvar,Nw

    Lmean =Emean
    count = 0
    if algorithm == 0:
        while True:
            count += 1
            Egamma = Expectationn(data,Emean,Evar,Ew)
            o = Maximization(data,Egamma)
            Emean,Evar,Ew  = o
            if np.all(np.abs(Lmean - Emean) < threshold):
                break
            Lmean = Emean
    else:
        def Maximization1(data,Egamma,epsilon=1e-9):
            assert len(Egamma.shape) == 2
            Nw = np.mean(Egamma,axis=1)
            ## only select the maximum value of eacho component
            B0 = np.select([Egamma[0]>Egamma[1]],[Egamma[0]], 0)
            B1 = np.select([Egamma[0]<Egamma[1]],[Egamma[1]], 0)
            Egamma = np.concatenate([[B0],[B1]],0)
            Nmean = np.sum(Egamma*data, -1) / (np.sum(Egamma,-1) + epsilon)
            Nvar = np.sum(Egamma*(data-np.expand_dims(Nmean,-1))**2,-1) / (np.sum(Egamma,-1)+epsilon)
            return Nmean,Nvar,Nw
        flag = True
        while True:
            count += 1
            Egamma = Expectationn(data,Emean,Evar,Ew)
            if flag:
                o = Maximization(data,Egamma)
            else:
                o = Maximization1(data,Egamma)
            Emean,Evar,Ew  = o
            if flag:
                if np.all(np.abs(Lmean - Emean) < threshold*10000):
                    flag=False
            else:
                if np.all(np.abs(Lmean - Emean) < threshold):
                    break
            Lmean = Emean        
    
    return Emean,Evar,Ew,count

def test_EM():
    c = C_Gaussian_onedimension()
    data = c.get_numpy_data(means=[0.,0],stds=[1.,1.],sizes=[1000,1000])
    c.plot_data(data)
    r = EM_univariate(data,2,algorithm=0)
    print("mean",r[0],"var",r[1])
    print("weight",r[2],"iteration count",r[3])

test_EM()
