#!-*-coding:utf-8-*-
import sys,os
import numpy as np
import copy
from tqdm import tqdm

## tqdm test
# def iteror():
#     for x in range(100):
#         yield x
# for x in tqdm(zip(range(10),iteror())):
#     print(x)
# exit()

class SA():
    """
    两种方法计算， __fallalign 和 __create_iteror,但是__fallalign消耗大量内存，只能进行小数量的全排列。
    """
    def __init__(self):
        self.iteror_v=[]
    def II(self,n):
        I=1
        for i in range(1,n+1):
            I *= i
        return I 
    def __fallalign(self,l):
        """
        策略：从后向前进行append
        """
        if len(l) == 1:
            return [l]
        toreturn=[]
        for i in l:
            #nl = copy.deepcopy(l)
            #print(nl.remove(i)) ## why this is None?
            nl = [x for x in l if x != i]
            r = self.__fallalign(nl)
            for ii in range(len(r)):
                r[ii].append(i)
            toreturn.extend(r)

        return toreturn

    def __create_iteror(self,length,l):
        """
          function is the same as __fallalign, but we use iteror here
        """
        if len(l) == 1:
            self.iteror_v.append(l[0])
            yield self.iteror_v
            return
        for i in l:
            self.iteror_v = self.iteror_v[0:length-len(l)]
            self.iteror_v.append(i)
            nl = [x for x in l if x != i]
            subiteror=self.__create_iteror(length,nl)
            for r in subiteror:
                yield r     ## 注意这里不是yield subiteror 

    def cal_fee(self,iteror):
        lastvalue = next(iteror)
        fee = len(lastvalue)
        for vlist in iteror:
            find_len =  len(vlist)
            for i in range(1,len(vlist)):
                if(lastvalue[-i] == vlist[0]):
                    find_len=len(vlist) - i
                    for ii in range(1,i):
                        if lastvalue[-ii] != vlist[i-ii]:
                            find_len=len(vlist)
                            break
                    break

            #print("find_len",find_len)
                
            fee += find_len
            lastvalue = vlist

        return fee

    def calc_percentlist(self,value):
        """
        测试只计算value / 1000000^n * 1000000^n (忽略一些尾数)的迭代。
        """
        value = int(value) 
        # print(value) 
        factor = 1000000  ## 1e6
        toreturn = []
        while True:
            if value > factor:
                value = value/factor  
                print(int(value))
                toreturn.append(factor)
            else:
                toreturn.append(int(value))
                break
        toreturn = toreturn[::-1]
        return toreturn
    
    def dynamic_iteor(self,_list,iteror):
        """
        从进度条现实的时间来看
        main(n), n=3的时候迭代(3x2x1)! = 720 小于1s
        main(n), n=4的时候迭代(4x3x2x1)!= 620448401733239439360000
                        大约需要: 16s*1e6*1e6*620488 /3600s/24d/365y = 314808726534.75397 (3千忆年) 
        """
        for _ in tqdm(range(_list[0]),desc="len:"+str(len(_list))):
            if len(_list) == 1:
                yield next(iteror)
            else:
                r = self.dynamic_iteor(_list[1:],iteror)
                for nr in r:
                    yield nr

    def main(self,n):
        _list = [x for x in range(n)]
        ## Test 1
        #r = self.__fallalign(_list)
        #print(r)
        ## Test 2
        #for v in self.__create_iteror(len(_list),_list):
        #    print(v)

        # Test cal fee 
        #leng = len(_list)
        #fee = self.cal_fee(self.__create_iteror(leng,_list))
        #print(fee)  ## max_len = fee

        # start
        first_list = [x for x in range(n)]
        aligned_list = []
        iteror = self.__create_iteror(len(first_list),first_list)
        for v in iteror:
            aligned_list.append(v)  # n=3, 3x2x1 list
        
        second_list = [x for x in range(len(aligned_list))]
        iteror2 = self.__create_iteror(len(second_list),second_list) # n=3, (3x2x1)! iteror
        iteror2_count = self.II(len(second_list))

        print("we will iter count:",iteror2_count)
        min_fee =  self.II(n)*n #(n!) * n
        min_list = []
        min_list_max_count = 100

        for v in self.dynamic_iteor(self.calc_percentlist(iteror2_count),iteror2):
            cal_list = []
            for indx in v:
                cal_list.append(aligned_list[indx])
            cal_iter = iter(cal_list)
            fee = self.cal_fee(cal_iter)
            if fee <= min_fee:
                if fee < min_fee:
                    min_list = []
                    min_fee = fee
                if len(min_list) < min_list_max_count:
                    min_list.append(cal_list)
            # print(fee)
            
        print("min_fee",min_fee)
        print("min_list",min_list,"length",len(min_list))



sa = SA()
sa.main(3)

"""
sum up:
    use iteror
    use 策略：从后向前进行append
    cal_fee: 计算相邻的差值。
To do:
    算法复杂度:


"""
