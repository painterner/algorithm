import numpy as np

def halves_ops(X,func,alg='N'):
    """
    sum 
        ____________
        | A    |    |
        |______|    |
        |\     \    |
        | \  B  \   |
        |  \_____\__|
        |   |  C    |
        |___|_______|
    """
    def isodd(x):
        return x % 2 == 1
    def iseven(x):
        return x % 2 == 0
    outputs = []
    assert X.shape[0] == X.shape[1]
    l = X.shape[0]
    win = l // 2
    win_12 = win // 2

    if l == 1 or l==2 or l==3:
        return np.sum(X,-1)
    if iseven(l) and iseven(win):
        win = win + 1
    elif isodd(l) and isodd(win):
        win = win + 2
        win_12 = win_12 + 1
    elif isodd(l) and iseven(win):
        win = win + 1
    else:
        pass    ## this the case iseven(l) and is isodd(win)
    win_32 = l-win_12
    win_last = l - win

    for i in range(l):
        if i < win_12:
            o =  X[i,:win] if alg == 'N' else X[i,:]
        elif i < win_32:
            o =  X[i,i-win_12:i+win_12+1]
        else:
            o =  X[i,win_last:] if alg == 'N' else X[i,:]
        outputs.append( o )
    outputs = np.array(outputs)
    outputs = func(outputs)
    return outputs


if __name__ == "__main__":
    a = np.random.randint(0,5,[20,20])
    o = halves_ops(a,np.sum)